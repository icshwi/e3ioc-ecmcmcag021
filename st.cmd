##############################################################################
## Example config ESS crate of type MCU1026

##############################################################################
## Initiation:
epicsEnvSet("IOC" ,"$(IOC="MCAG-021:Ctrl-ECAT-01")")
epicsEnvSet("ECMCCFG_INIT" ,"")  #Only run startup once (auto at PSI, need call at ESS), variable set to "#" in startup.cmd
epicsEnvSet("SCRIPTEXEC" ,"$(SCRIPTEXEC="iocshLoad")")

epicsEnvSet("NAMING","ESSnaming")

require ecmccfg,7.0.1
#require essioc

#- Choose motor record driver implementation
#-   ECMC_MR_MODULE="ecmcMotorRecord"  => ECMC native built in motor record support (Default)
#-   ECMC_MR_MODULE="EthercatMC"       => Motor record support from EthercatMC module (need to be loaded)
#- Uncomment the line below to use EthercatMC (and add optional EthercatMC_VER to startup.cmd call):
#- epicsEnvSet(ECMC_MR_MODULE,"EthercatMC")

# Epics Motor record driver that will be used:
# epicsEnvShow(ECMC_MR_MODULE)

# run module startup.cmd (only needed at ESS  PSI auto call at require)
$(ECMCCFG_INIT)$(SCRIPTEXEC) ${ecmccfg_DIR}startup.cmd, "IOC=$(IOC),ECMC_VER=7.0.1"

##############################################################################

# Run essioc snippet
#iocshLoad("$(essioc_DIR)common_config.iocsh")

# Configure hardware:
ecmcFileExist($(E3_CMD_TOP)/hw/ecmcMCU1026.cmd,1)
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcMCU1026.cmd

# Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"

# ADDITIONAL SETUP
# Set all outputs to feed switches
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput01,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput02,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput03,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput04,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput05,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput06,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput07,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},binaryOutput08,1)"
# END of ADDITIONAL SETUP

##############################################################################
## AXIS 1
#
epicsEnvSet("DEV",      "$(IOC)")
$(SCRIPTEXEC) ($(ecmccfg_DIR)configureAxis.cmd, CONFIG=$(E3_CMD_TOP)/cfg/linear_1.ax)

##############################################################################
## AXIS 2
#
epicsEnvSet("DEV",      "$(IOC)")
$(SCRIPTEXEC) ($(ecmccfg_DIR)configureAxis.cmd, CONFIG=$(E3_CMD_TOP)/cfg/linear_2.ax)

##############################################################################
############# Configure diagnostics:

ecmcConfigOrDie "Cfg.EcSetDiagnostics(1)"
ecmcConfigOrDie "Cfg.EcEnablePrintouts(0)"
ecmcConfigOrDie "Cfg.EcSetDomainFailedCyclesLimit(100)"
ecmcConfigOrDie "Cfg.SetDiagAxisIndex(1)"
ecmcConfigOrDie "Cfg.SetDiagAxisFreq(2)"
ecmcConfigOrDie "Cfg.SetDiagAxisEnable(0)"

# go active
$(SCRIPTEXEC) ($(ecmccfg_DIR)setAppMode.cmd)


iocInit()

