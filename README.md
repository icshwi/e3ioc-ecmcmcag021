# e3ioc-ecmcmcu1026

e3 ioc - EtherCAT Motion Control MCU 1026

## Note about the MCU 1026

It has a relay to the power (+24 v) of the second EL7037 (axis 2) which is closed when axis 1 is in the low limit switch. So axis 2 will **ONLY** move if axis 1 is in low limit switch.

## Cloning

Clone this IOC with `git clone --recurse-submodules https://gitlab.esss.lu.se/javiercereijogarcia/e3ioc-ecmcmcu1026.git`.

**Note:** Don't set the `--recurse-submodules` if you are not interested in the opis.

## Running the IOC

This IOC doesn't need to be compiled nor installed in a specific directory. Just make sure that the versions of `ecmccfg`, `ecmc` and `stream` (also `EthercatMC` if you want to use it instead of the default ECMC native built in motor record support) in `st.cmd` are installed for the EPICS base and `require` version that you are using.

If your E3 environment is activated, just go to the top directory of this IOC and run `iocsh.bash st.cmd`.

## History

Initially this IOC was located in
https://github.com/icshwi/ecmccfg/tree/master/examples/mcu1026 .

